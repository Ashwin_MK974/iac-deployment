provider "google" {
  credentials = file("carbon-nucleus-345204-478b4da9758d.json")
  project = "carbon-nucleus-345204"
  region = "europe-west1"
  zone = "europe-west1-b"
}

resource "google_compute_instance" "terraform" {
  project      = "carbon-nucleus-345204"
  name         = "solana-dev-server"
  machine_type = "e2-standard-4"
  zone         = "us-west4-b"
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
  metadata_startup_script = <<EOF
#!/bin/bash
apt update && apt install wget -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/install.sh && chmod +x install.sh && bash install.sh && . ~/.bashrc
EOF

depends_on = [ google_compute_firewall.firewall, google_compute_firewall.webserverrule ]
}

resource "google_compute_firewall" "webserverrule" {
  name    = "gritfy-webserver"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80","443","3000","8999","8900"]
  }
  source_ranges = ["0.0.0.0/0"] # Not So Secure. Limit the Source Range
  target_tags   = ["webserver"]
}


resource "google_compute_firewall" "firewall" {
  name    = "gritfy-firewall-externalssh"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["0.0.0.0/0"] # Ici on autorise les connexion SSH à partir de n'importe qu'elle adresse IP
  target_tags   = ["externalssh"] #Toutes les vm qui ont la même target_tags appliquent les règles de parefeu.
}


