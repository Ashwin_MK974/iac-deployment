#!/bin/bash
#gcloud config set project PROJECT-ID
#Author Ashwin MK

VERSION=" Ultra Beta 0.0.3xx --n --stable"
#Définition des fonctions de vérifications
function success_write(){
    echo ""
    echo -e "\e[1m$1 ✔️\e[0m"
    echo -e "\e[97m"
}
function fail_write(){
   echo ""
   echo -e "\e[1m$1 ❌\e[0m"
   echo -e "\e[97m"
}
function green_write(){
    echo ""
    echo -e "\e[32m$1"
    echo -e "\e[97m"
}
function green_write_sucess(){
    echo ""
    echo -e "\e[32m$1✔️"
    echo -e "\e[97m"
}

function important_write(){
    echo ""
    echo -e "\e[1m$1\e[0m"
    echo -e "\e[97m"
}

function get_answer() 
{
     echo ""
    echo -e "\e[1m$1 🔍\e[0m"
    echo -e "\e[97m"   
}

function warning_write(){
    echo ""
    echo -e "\e[1m""⚠️ "  $1 "\e[0m"
    echo -e "\e[97m"
}
function italic_write(){
    echo ""
    echo -e "\033[3m$1\033[23m"
    echo -e "\e[97m"
}
function italic_warning_write(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}
function italic_write_warning(){
    echo ""
    echo -e "\033[3m$1 ⚠️\033[23m"
    echo -e "\e[97m"
}

function verify_existant_element__v3(){
    if [ -$1 $2 ]
    then
    if [ "$5" == "--v" ]
    then
        success_write "L'élement $2 existe"
        if [ "$6" != ""]
        then
          echo $6
        fi
    fi
        return $3
    else
    if [ "$5" == "--v" ]
    then
        fail_write "L'élement $2 n'existe pas"
        if [ "$6" != "" ]
        then
          echo $6
        fi
    fi

        return $4
    fi
}

function debug()
{
    for (( c=1; c<=$#; c++ ))
    do
         echo "$"$c"===============================================> ${!c} "
    done
}


function require()
{

    #echo "Nombre argument $#"
    function require_process()
    {
            #process=$($1 2>&1)
            #echo "==========>$x"
            if [ "$2" == "" ]
            then
                    if ! [ -x "$(command -v $1)" ]
                    #if [[  "$process" == *"not found"*  || "$process" == *"bash: /usr/bin/"* ]]
                    then
                        echo "La commande  $1 n'existe pas" 
                        #apt update &&
                        #apt install $1 -y
                        
                    else
                        echo "La commande $1 existe" 
                    fi
            else
                echo "do process"
            fi

    }
    for (( c=1; c<=$#; c++ ))
    do
         require_process "${!c}"
    done
}

function requires(){
        function subprocess()
        {
            verify_state "$1_install:true" 1 0 $1 --v && apt install $1 -y
        }
        for (( c=1; c<=$#; c++ ))
        do
            subprocess "${!c}"
        done
}

function verify_state(){
                    #Mise à jour du fichier state pour les exécution des prochaines fonctions
                    if [ -e /etc/clouder/clouder_state ]
                    then
                                    if grep -Fxq $1 /etc/clouder/clouder_state
                                    then
                                            if [[ $5 == "--v" ]]
                                            then
                                                    green_write_sucess "$4 à déjà été installé"
                                            fi
                                            return $2
                                    else
                                            warning_write "Mise à jour du fichier"
                                            echo $1 >> /etc/clouder/clouder_state
                                            green_write_sucess "Fichier mise à jour"
                                            return $3     
                                    fi
                    else
                                    if [ -d /etc/clouder ]
                                    then
                                    (cd /etc/clouder ; touch clouder_state)
                                    echo $1 >> /etc/clouder/clouder_state
                                    green_write_sucess "Fichier mise à jour"
                                    return $3
                                    else
                                        mkdir /etc/clouder
                                        (cd /etc/clouder ; touch clouder_state)
                                        echo $1 >> /etc/clouder/clouder_state
                                        green_write_sucess "Fichier mise à jour"
                                        return $3
                                    fi
                    fi
}



function sourcelist_res(){
    verify_existant_element__v3 d /etc/clouder/backup/sourcelist 0 1 --s && cp /etc/clouder/backup/sourcelist/sources.list /etc/apt/sources.list &&
    cp -r  /etc/clouder/backup/sourcelist/sources.list.d/   /etc/apt/ && green_write_sucess "Restauration des fichiers sourcelist effectué avec succès"
}

function restaure()
{
    if [ $1 == "sourcelist" ]
    then
        sourcelist_res
    elif [ $1 == "xxxx" ]
    then
        echo "no process"
    else
        echo "no process"
    fi
}
env="google-env"

    function clouder_init()
    {
        if [ "$env" == "google-env" ]
        then
            if [[ $1 == "install"  || $1 == "i" ]]
            then
                    install_struct $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21}
            elif [[ $1 == "update"  || $1 == "u" ]]
            then
                clouder_update $2 $3 $4
            elif [[ $1 == "--version"  || $1 == "--v" ]]
            then
                versioning
            elif [[ $1 == "rinitialize"  || $1 == "rinit" ]]
            then
                clouder_rinit $2
            elif [[ $1 == "scale" ]]
            then
                scaling $2 $3 $4 $5
            elif [[ $1 == "deploys" ]]
            then
                deploy_v2 $2 $3 $4 $5 $6
            elif [[ $1 == "destroys" ]]
            then
                destroy_v2 $2 $3 $4 $5 $6
            elif [[ $1 == "--info"  || $1 == "--i" ]]
            then
                info
            elif [[ $1 == "show" ]]
            then
                show_struct $2 $3 $4 $5
            elif [[ $1 == "auto-config"  ]]
            then
                auto_config_process $2 $3 $4 $5 $6 $7 $8 $9
            elif [[ $1 == "create"  ]]
            then
            create $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21}
            elif [[ $1 == "rj"  ]]
            then
                rj $2 $3
            elif [[ $1 == "res"  ]]
            then
                restaure_list_accounts_default
            elif [[ $1 == "manage"  ]]
            then
                manage $2 $3 $4 $5 $6 $7 $8 $9
            elif [[ $1 == "uj"  ]]
            then
                uj $2 $3 $4
            elif [[ $1 == "deploy"  ]]
            then
                deploy_struct $2 $3 $4
            elif [[ $1 == "destroy"  ]]
            then
                destroy_struct $2 $3 $4
            elif [[ $1 == "get"  ]]
            then
                get_struct $2 $3 $4 $6
            elif [[ $1 == "restaure"  ]]
            then
            sourcelist_res $2 $3 $4 $5 $6
            elif [[ $1 == "set-env"  ]]
            then
              set_env $2 $3 $4
            elif [[ $1 == ""  ]]
            then
                green_write_sucess "0 Erreur"
            else
                    fail_write " L'argument $1 n'existe pas "
            fi
        else
            echo "Azure env detected"
            if [[ $1 == "install"  || $1 == "i" ]]
            then
                    install_struct $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21}
            elif [[ $1 == "update"  || $1 == "u" ]]
            then
                clouder_update $2 $3 $4
            elif [[ $1 == "--version"  || $1 == "--v" ]]
            then
                versioning
            elif [[ $1 == "show" ]]
            then
                show_struct $2 $3 $4 $5
            elif [[ $1 == "set-env"  ]]
            then
              set_env $2 $3 $4
            elif [[ $1 == "rinitialize"  || $1 == "rinit" ]]
            then
                clouder_rinit $2
            fi
        fi
    }

function set_env()
{
    if [ $1 == "google" ]
    then
        sed -i "s/azure-env/google-env/" /etc/clouder/clouder.sh && echo "set on goole"
    else
        sed -i "s/google-env/azure-env/" /etc/clouder/clouder.sh && echo "set on azure"
    fi
}

function get_struct(){
    if [[ $1 == "own" ]]
    then
        chmod -R 777 /etc/clouder
        green_write_sucess "Obtention des droits réalisés avec succès"
    fi
}
function clouder(){
    bash /etc/clouder/./clouder.sh clouder_init $1 $2 $3 $4 $5
}
function init_gcloud_config(){
    if [ -d /etc/clouder/gcp ]
    then
        $1
    else
        mkdir /etc/clouder/gcp
        (cd /etc/clouder/gcp ; mkdir passwd)
    fi
}
function versioning()
{
    success_write "Version : $VERSION" 
}
function info()
{
    important_write "Author : Ashwin MK"
    success_write "Version :  $VERSION" 
}


function clouder_rinit()
{
    function display_info(){
        important_write "Le fichier clouder_state à été réinitialisé"
    }
    if [ -z "$1" ]
    then
                    italic_warning_write "Souhaitez-vous rinitialiser le fichier clouder_state ?"
                    read r
                    if [[ $r == "y" || $r == "Y" || $r == "yes" || $r == "YES"  ]]
                    then
                        echo "" > /etc/clouder/clouder_state
                        display_info
                    else
                        italic_write "Opération annulé"
                    fi
    elif [[ $1 == "--force" || $1 == "--f" ]]
    then
                    echo "" > /etc/clouder/clouder_state
                    display_info
    else
        warning_write "L'argument $1 n'est pas référencé  dans Clouder Init "
    fi
}




function rj(){ #read jsonFile
    jq -r ".$1" /etc/clouder/gcp/accounts/list_accounts.json
}




function uj(){ #Mise à jour du fichier JSON 
    jq --arg newval "$2"  '."'"$1"'" |= $newval' /etc/clouder/gcp/accounts/list_accounts.json > tmp_data.json
    cat tmp_data.json > list_accounts.json
    cat list_accounts.json
    green_write_sucess "Opération effectué avec succès"
}

#rj "name" => Nous permet d'obtenir la valeur
#uj "name" "amk" => Permet de créer ou de mettre à jour la valeur


function restaure_list_accounts_default()
{
cat > /etc/clouder/gcp/accounts/list_accounts.json << 'EOL'
{
  "global_activated_account":false,
  "number_accounts" : 0,
  "account_0" : "/etc/clouder/gcp/organisation"
}
EOL
green_write "Restauration effectué"
cat /etc/clouder/gcp/accounts/list_accounts.json
}





function test(){
if [ $(rj "global_activated_account") == "false" ]
then
#La fonction doit déjà vérifier si gcloud est configuré est installé
    uj "global_activated_account" "true"
     echo "Compte activé"
    
else
    echo "Compte déjà activé"
fi  
}


function deploy_struct
{
    if [[ $1 == "default" && $2 == "mode" ]]
    then
        important_write "Exécution du process de déploiement 1 "
        (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -auto-approve )
        green_write_sucess "Déploiement de l'environnement par défaut réussie "
    elif [[ $1 == "firewall" && $2 == "devellopement" ]]
    then
        important_write "Exécution du process de déploiement 2"
        (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_firewall.webserverrule -auto-approve )
        green_write_sucess "Déploiement de l'environnement par défaut réussie "
    elif [[ $1 == "minimal" && $2 == "instance" ]]
    then
        important_write "Exécution du process de déploiement 3 "
        (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_instance.minimal -auto-approve )
        green_write_sucess "Déploiement de l'environnement par défaut réussie "
    elif [[ $1 == "server" && $2 == "manager" && $3 == "" ]]
    then
        important_write "Exécution du process de déploiement 4 "
         (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_instance.ultrabasic -auto-approve )
        green_write_sucess "Déploiement de l'environnement par défaut réussie "
    elif [[ $1 == "server" && $2 == "test" && $3 == "medium" ]]
    then
        important_write "Exécution du process de déploiement 4 "
         (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_instance.medium -auto-approve )
        green_write_sucess "Déploiement de l'environnement par défaut réussie "
    elif [[ $1 == "firewall" && $2 == "devellopment" && $3 == "mode" ]]
    then
          important_write "Exécution du process de déploiement 5"
         (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_firewall.webserverrule -auto-approve )
        green_write_sucess "Déploiement de l'environnement par défaut réussie "
    elif [[ $1 == "server" && $2 == "manager" && $3 == "ultra" ]]
    then
        important_write "Exécution du process de déploiement 5"
         (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_instance.ultrax -auto-approve )
        green_write_sucess "Déploiement de l'environnement par défaut réussie "

    elif [[ $1 == "server" && $2 == "debian" && $3 == "" ]]
    then
        important_write "Exécution du process de déploiement 5"
         (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_instance.debian -auto-approve )
        green_write_sucess "Déploiement de l'environnement réussie "
    else
        important_write "Fonction en cours de construction :/"
        italic_write "Certaines fonction n'ont pas encore été programmer totalement"
    fi
    (cd /etc/clouder/gcp/accounts ; terraform show | grep nat_ip | sed 's/nat_ip/PUBLIC IP/g' | sed 's/   //g') #Afin de voir l'adresse IP publique
}

function destroy_struct(){
    if [[ $1 == "default" && $2 == "mode" ]]
    then
        (cd /etc/clouder/gcp/accounts ; terraform destroy -auto-approve )
        green_write_sucess "Infrastructure détruite "
    elif [[ $1 == "minimal" && $2 == "instance" ]]
    then
        (cd /etc/clouder/gcp/accounts ; terraform init && terraform destroy -target=google_compute_instance.minimal -auto-approve )
        green_write_sucess " Infrastructure détruite "
    elif [[ $1 == "server" && $2 == "manager"  && $3 == "" ]]
    then
        (cd /etc/clouder/gcp/accounts ; terraform init && terraform destroy -target=google_compute_instance.ultrabasic -auto-approve )
        green_write_sucess "Infrastructure détruite "
    elif [[ $1 == "server" && $2 == "manager" && $3 == "ultra" ]]
    then
        (cd /etc/clouder/gcp/accounts ; terraform init && terraform destroy -target=google_compute_instance.ultrax -auto-approve )
        green_write_sucess "Infrastructure détruite "
    elif [[ $1 == "server" && $2 == "debian" && $3 == "" ]]
    then
        important_write "Exécution du process de déploiement 5"
         (cd /etc/clouder/gcp/accounts ; terraform init && terraform destroy -target=google_compute_instance.debian -auto-approve )
        green_write_sucess "Destruction  de l'environnement réussie "
    elif [[ $1 == "server" && $2 == "test" && $3 == "medium" ]]
    then
        (cd /etc/clouder/gcp/accounts ; terraform init && terraform destroy -target=google_compute_instance.medium -auto-approve )
        green_write_sucess "Infrastructure détruite "
    else
        important_write "Fonction en cours de construction :/"
    fi
}


#e2-standard-4

function rj_v2() #read json file
{
    jq -r ".$1" $2  
}

function CHECK_SERVICE_API
{
        CHECK=$(gcloud services list --enabled --project $1 | grep $2)
        if [  ! -z "$CHECK" ]
        then
            important_write "Le service API est déjà installé" &&
            return 0
        else
            important_write "Le service API n'est pas installé" &&
            important_write "Installation du service $2" &&
            gcloud services enable $2 && return 0 || fail_write "Une erreur est survenue au niveau de CHECK_SERVICE_API " && italic_warning_write "Suppresion des informations "  && fail_write "Le projet ne peut être ajouté , veuiller vérifier les droit du fichier clé"  &&
            if [ -d /etc/clouder/gcp/accounts/$1 ];then rm -r /etc/clouder/gcp/accounts/$1;fi && return 1
        fi

}


function uniformize_ressources
{

<<COMMENT
    Cette fonction permet à partir du fichier de clé de récupérer des valeur en exécutant
    le cli de gcp pour uniformiser le fichier terraform de telle sorte que ca soit prêt à
    démarrer
COMMENT

    PROJECT_NAME=$(rj_v2 "project_id" /etc/clouder/gcp/accounts/key.json)
    echo "========> $PROJECT_NAME "
    sed -i "s/xxxx-PROJECT-ID-xxxx/$PROJECT_NAME/g" /etc/clouder/gcp/accounts/main.tf
    if [ -d /etc/clouder/gcp/accounts/$PROJECT_NAME ]
    then
        echo "Project Already Init"
    else
    (cd /etc/clouder/gcp/accounts ; mkdir $PROJECT_NAME)
    (cd /etc/clouder/gcp/accounts/$PROJECT_NAME ; mv /etc/clouder/gcp/accounts/key.json /etc/clouder/gcp/accounts/$PROJECT_NAME/key.json )
    fi

    gcloud config set project $PROJECT_NAME
    #CHECK_SERVICE_API $PROJECT_NAME compute.instances.list
    CHECK_SERVICE_API $PROJECT_NAME compute.googleapis.com && 
    CHECK_SERVICE_API $PROJECT_NAME cloudresourcemanager.googleapis.com &&
    REGION=$(gcloud config get-value compute/region) &&
    ZONE=$(gcloud config get-value compute/zone) && 
    KEY_PATH="\/etc\/clouder\/gcp\/accounts\/$PROJECT_NAME\/key.json" && 
    if [ "$REGION" == "" ]
    then
        echo "Aucune région n'a été définie"
        important_write "Définition d'une zone et région par défauts"
        sed -i "s/xxx-PROJECT-REGION-xxx/europe-west1/g" /etc/clouder/gcp/accounts/main.tf
        sed -i "s/xxx-PROJECT-ZONE-xxx/europe-west1-b/g" /etc/clouder/gcp/accounts/main.tf
        gcloud compute project-info add-metadata --metadata google-compute-default-region=europe-west1,google-compute-default-zone=europe-west1-b
        #gcloud init
    else
        sed -i "s/xxx-PROJECT-REGION-xxx/$REGION/g" /etc/clouder/gcp/accounts/main.tf
        sed -i "s/xxx-PROJECT-ZONE-xxx/$ZONE/g" /etc/clouder/gcp/accounts/main.tf
    fi && 
    sed -i "s/credential_file_path.json/$KEY_PATH/g" /etc/clouder/gcp/accounts/main.tf || return 1

    #PROJECT_NAME=$(gcloud compute project-info describe | grep name: | sed 's/name://g' | sed 's/ //g')
}


function auto_config_process()
{
    #debug $1 $2 $3 $4 $5 $6 $7 $8 $9
    init_gcloud_config &&  
    process_create $3 &&
    if [[ $1 == "gcloud" &&  $2 == "from"  && ! -z $3 ]]
    then
        echo "Fichier clé du projet renseigné : $3"
        for_auto_config $3
        
    else
            echo "Erreur de syntaxe au niveau de la commande clouder auto-config"
    fi
}
function for_auto_config()
{
    function reprocess() {
            echo "======================================> $1"
            echo "======================================> $2"
            if [ -d /etc/clouder/gcp/accounts ]
            then
                process=$(cd /etc/clouder/gcp/accounts ; $1 $2 )
                        if [[  "$process" == *"invalid"*  || $? == 1 ]]
                        then
                            fail_write "Une erreur est survenue ↖" && return 1
                        fi
                    if [ -e /etc/clouder/gcp/accounts/key.json ]
                    then
                        cd /etc/clouder/gcp/accounts && rm key.json
                    fi
                    (cd /etc/clouder/gcp/accounts ; mv *.json key.json && gcloud auth activate-service-account --key-file key.json)
                    uniformize_ressources && 
                    green_write_sucess  "Le projet à correctement été initialisé"
            else
                mkdir /etc/clouder/gcp/accounts
                (cd /etc/clouder/gcp/accounts; touch list_accounts.json)
            fi
    }
    
    if [[ "$1" == *"http"*   ]]
    then
            if [[ "$1" == *"https"* ]]
            then
                important_write "Utilisation du protocole  https"
            else
                important_write "Utilisation du protocole  http"
            fi
            if [[ "$1" == *"mega"*   ]]
            then
                requires megatools
                reprocess megadl $1
            elif [[ "$1" == *"google"* ]]
            then
                important_write "Utilisation du process http google"
            else
                fail_write "Une erreur est survenue, Clouder ne prend pas en charge ce nom de domaine"
            fi
    else
         echo "Utilisation de chemin relatif car pas de protocole http/https détecté"
         verify_existant_element__v3 e $1 0 1 && 
         cp $1 /etc/clouder/gcp/accounts/key.json && 
         cd /etc/clouder/gcp/accounts && gcloud auth activate-service-account --key-file key.json && 
         uniformize_ressources &&
         green_write_sucess  "Le projet à correctement été initialisé"
    fi
    

}


function scaling()
{
    #debug $1 $2 $3 $4 $5 $6 $7 $8 $9
    if [[ $1 == "--help" && $2 == "" && $3 == "" ]]
    then
        italic_write "ScalingX est un programme permettant de faire de l'hyperScalling ainsi que du ReverseScaling \n
        Concu dans le but de faire utiliser uniquement les ressources dans un état minimale ou maxi
        La version de ScalingX n'utilise pas Terraform, mais les API SDK de Google Cloud
        "
    elif [[ $1 == "reverse" && $2 == "max" ]]
    then
         echo "no process"
    elif [[ $1 == "--show" && $2 == "project" && $3 == "machine" ]]
    then
    echo "-----------------------Liste de machine du projet-------------------------------------"
    gcloud compute instances list
    elif [[ $1 == "--show" && $2 == "zone" && $3 == "machine" ]]
    then
    echo "-----------------------Liste de machine disponible-------------------------------------"
    gcloud compute machine-types list --filter="zone:( europe-west1-b )"
    elif [[ $1 == "reverse" && $2 == "ultra" && $3 != "" ]]
    then
            gcloud compute instances set-machine-type $1 --machine-type e2-micro && gcloud compute instances start $1  || echo "Une erreur est survenue" && return 1 
    elif [[ $1 != "" && $2 == "on" && $3 != "" ]]
    then
            gcloud compute instances set-machine-type $1 --machine-type $3  || echo "Une erreur est survenue" && return 1 
    else
        important_write " ScalingX : Erreur au niveau des commandes fournies"
        
    fi
    #clouder scale server-manager-ultrax on 
}


function required__for_create()
{
    #Une sorte de required
    verify_existant_element__v3 d /etc/clouder/gcp/accounts/terraform_manage 1 0 && 
    cd /etc/clouder/gcp/accounts/ && mkdir terraform_manage
    verify_existant_element__v3 d /etc/clouder/gcp/accounts/terraform_manage/model 1 0 && 
    cd /etc/clouder/gcp/accounts/terraform_manage && mkdir model
    #verify_existant_element__v3 e  /etc/clouder/gcp/accounts/terraform_manage/model/$1--model 1 0 --v "Le modèle à déjà été créer"
}
function show_struct()
{
    if [ $1 == "model" ]
    then
          important_write "Liste des modèle : "
          important_write "$(ls /etc/clouder/gcp/accounts/terraform_manage/model)"

    elif [ $1 == "env" ]
    then
            echo "L'environnement définie est sur $env"
    else
            echo "no process"
    fi
}
function create()
{
    PROJECT_NAME=$(cat /etc/clouder/clouder_state | grep project_define | sed "s/project_define://g") &&
    if [ -z $PROJECT_NAME ]
    then
        fail_write "Impossible d'utiliser la fonction create car aucun projet n'est définie" && 
        italic_write "Pour définir un projet utiliser la commande clouder manage set project [PROJECT_NAME] " &&
        italic_write "Projet trouver : "
        manage show projects && 
        return 1
    fi &&
    export origin=/etc/clouder/gcp/accounts/terraform_manage/model
    #debug $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15} ${16} ${17}
    if [[ $2 == "model" && $3 == "--name" && $4 != "" && $5 == "--vm-name" && $6 != ""  && $7 == "--machine-type" && $8 != "" && $9 == "--zone"  && ${10} != "" && ${11} == "--server-tags" && ${12} != ""  && ${13} == "--disk-size" && ${14} != "" && ${15} == "--image-source" &&  ${16} != ""   ]]
    then
    important_write "====================================================>Creation  de VM"
    required__for_create $4
    verify_existant_element__v3 e  "/etc/clouder/gcp/accounts/terraform_manage/model/$4" 0 1 && warning_write "Le modèle existe déjà" && return 0
    cat >> /etc/clouder/gcp/accounts/terraform_manage/model/$4 <<EOF
    resource "google_compute_instance" "$4" {
    project      = "$PROJECT_NAME"
    name         = "$6"
    machine_type = "${8}"
    zone         = "${10}"
    tags = ["${12}"]
    boot_disk {
        initialize_params {
        image = data.google_compute_image.debian.self_link
        size = "${14}"
        }
    }
  network_interface {
    network = "default"
    access_config {
    }
  }
}
EOF
    important_write "Création du modèle effectué avec succès"
    elif [[ $2 == "model" && $3 == "--help" && $4 == "" ]]
    then
    important_write "Exemple de commande pour créer un  modèle"
    italic_write  "clouder create model --name myDefaultModel --project-name plucky-sight-363107  --vm-name manager-ultra-plus --machine-type e2-micro --zone europe-west1-b --server-tags testserver --disk-size 180 --image-source debian"
    elif [[ $2 == "image" && $3 == "" && $4 == "" ]]
    then
        italic_write "Process pour créer de nouvelle images"
    elif [[ $2 == "vm" && $3 != "" && $4 == "based" && $5 == "on" && $6 != "" && $7 == "" && $8 == "" ]]
    then
    #"\""$3"\""é
                if [ -e /etc/clouder/gcp/accounts/main.tf ]
                then
                    if grep -Fxq "#$3_vm_name_defined" /etc/clouder/gcp/accounts/main.tf
                    then
                    fail_write "Le nom de la machine virtuelle $3 existe déjà"
                    return 1
                    else
                    success_write "Création de la configuration de la VM"
                    fi
                fi
        if [ -e "/etc/clouder/gcp/accounts/terraform_manage/model/$6" ]
        then
                success_write "Le modèle existe"
        else
                fail_write "Le modèle n'existe pas, veuillez en créer un" && exit 1
        fi
        file=$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6)
        vm_name=$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 3p | awk '{print $3}' | sed 's/"//g' )
        machine_type=$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 4p | awk '{print $3}' | sed 's/"//g' )
        disk_size=$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 10p | awk '{print $3}' | sed 's/"//g' )
        project="$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 2p | awk '{print $3}' | sed 's/"//g' )"
        zone="$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 5p | awk '{print $3}' | sed 's/"//g' )"
        tags="$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 6p | awk '{print $3}' | sed 's/"//g' | sed 's/\[//g' | sed 's/\]//g'  )"
        image="$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 9p | awk '{print $3}' | sed 's/"//g' )"
        path_file="/etc/clouder/gcp/accounts/terraform_manage/model/$6_prepare"
        ressource="$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6 | sed -n 1p | awk '{print $3}' | sed 's/"//g' )"
        #echo $ressource
        sed "s/$vm_name/$3/g" <<< $file > /etc/clouder/gcp/accounts/terraform_manage/model/$6_prepare
        sed -i "s/$ressource/$3-vm-instance/g" $path_file
        #Afin d'eviter les if $1 == "--name" || $2 == "--name" etc...
        function verify_variable()
        {
            if [ "$1" == " " ]
            then
                echo "\$var is empty"
            else
                echo "\$var is NOT empty"
            fi

            #STR=$1
            #SUB='--'
            #if [[ "$STR" == *"$SUB"* ]]; then
            #fail_write "La valeur pour l'option $2 ne peut pas être le nom d'une autre option "
            #italic_warning_write " Vous avez oublier de préciser la valeur de l'option "
            #return 0
            #fi
        }
        for (( c=1; c<=$#; c++ ))
        do
            if [[ ${!c} == "--machine-type" ]]
            then
                i=$((c+1))
                #echo "=================================>${!i}"
                verify_variable ${!i} ${!c}
                sed -i "s/$machine_type/${!i}/g" $path_file
            elif [[ ${!c} == "--disk-size" ]]
            then
                i=$((c+1))
                verify_variable ${!i} ${!c}
                sed -i "s/$disk_size/${!i}/g" $path_file
            elif [[ ${!c} == "--project" ]]
            then
                i=$((c+1))
                verify_variable ${!i} ${!c}
                sed -i "s/$project/${!i}/g" $path_file
            elif [[ ${!c} == "--zone" ]]
            then
                i=$((c+1))
                verify_variable ${!i} ${!c}
                sed -i "s/$zone/${!i}/g" $path_file
            elif [[ ${!c} == "--tags" ]]
            then
                i=$((c+1))
                verify_variable ${!i} ${!c}
                sed -i "s/$tags/${!i}/g" $path_file
            elif [[ ${!c} == "--image" ]]
            then
                i=$((c+1))
                verify_variable ${!i} ${!c}
                sed -i "s/$image/${!i}/g" $path_file
            else
                STX=""
            fi
        done
        CONTENT=$(cat /etc/clouder/gcp/accounts/terraform_manage/model/$6_prepare)
        echo "#$3_vm_name_started_config" >> /etc/clouder/gcp/accounts/main.tf
        echo "$CONTENT" >> /etc/clouder/gcp/accounts/main.tf
        echo "#$3_vm_name_defined" >> /etc/clouder/gcp/accounts/main.tf
        rm  "/etc/clouder/gcp/accounts/terraform_manage/model/$6_prepare"
        for (( c=1; c<=$#; c++ ))
        do
            if [[ ${!c} == "--deploy" ]]
            then
                i=$((c+1))
                echo "Déploiement de la machine"
                target="google_compute_instance.$3-vm-instance"
                echo $target
                (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=$target -auto-approve )
            fi
        done
    else
        echo "Une erreur est survenue 813" && return 1 
    fi
}

function deploy_v2()
{
    target="google_compute_instance.$1-vm-instance"
    echo $target
    (cd /etc/clouder/gcp/accounts ; terraform init && terraform apply -target=google_compute_instance."$1-vm-instance" -auto-approve )
}
function destroy_v2()
{
    target="google_compute_instance.$1-vm-instance"
    echo $target
    (cd /etc/clouder/gcp/accounts ; terraform init && terraform destroy -target=$target -auto-approve ) 
}


function manage()
{
    verify_existant_element__v3 e /etc/clouder/gcp/accounts/main.tf 0 1 --s && get_reference_destination_old_path=$(cat /etc/clouder/gcp/accounts/main.tf | grep "#project_define:" | sed "s/#project_define://g")
    #debug $1 $2 $3 $4 $5 $6 $7 $8 $9
    if [[ $1 == "show" &&  $2 == "vm" && $3 == "" ]]
    then
    echo "-----------------------Liste de machine du projet-------------------------------------"
    gcloud compute instances list
    elif [[ $1 == "show" &&  $2 != "" && $3 == "info" && $4 == "on" && $5 == "zone" && $6 != "" ]]
    then
         gcloud compute instances describe $2 --zone $6 --format="yaml(name,status,disks)"
         #clouder manage show instance-1 info on zone us-west4-b
    elif [[ $1 == "show" &&  $2 != "" && $3 == "info"  ]]
    then
         gcloud compute instances describe $2  --format="yaml(name,status,disks)"
         #clouder manage show instance-1 info on zone us-west4-b
    elif [[ $1 == "start" && $2 != "" ]]
    then
        important_write "Démmarage de la vm $2"
        gcloud compute instances start $2
    elif [[ $1 == "change" && $2 == "region" && $3 == "and" && $4 == "zone" && $5 == "by" && $6 != "" && $7 != "" ]]
    then  
        gcloud compute project-info add-metadata --metadata google-compute-default-region=$6,google-compute-default-zone=$7
        gcloud init
    elif [[ $1 == "show" && $2 == "projects" || $1 == "show" && $2 == "project"  ]]
    then 
        ACTUAL_PROJECT=$(cat /etc/clouder/clouder_state | grep project_define | sed "s/project_define://g")
        RESULT=""
        if [ -z $ACTUAL_PROJECT ]
        then
            RESULT=$(ls /etc/clouder/gcp/accounts | sed 's/main.tf//g' | sed 's/terraform.tfstate//g' | sed 's/terraform_manage//g' |  sed 's/.backup//g' |  sed '/^$/d')
        else
           RESULT=$(ls /etc/clouder/gcp/accounts | sed 's/main.tf//g' | sed 's/terraform.tfstate//g' | sed 's/terraform_manage//g' |  sed 's/.backup//g' | sed "s/$ACTUAL_PROJECT/☛ $ACTUAL_PROJECT/g" |  sed '/^$/d')
        fi
        important_write "$RESULT"
    elif [[ $1 == "show" && $2 == "zone"   ]]
    then 
        gcloud config get-value compute/zone
    elif [[ $1 == "show" && $2 == "region"   ]]
    then 
        gcloud config get-value compute/region
    elif [[ $1 == "stop" && $2 != "" && $3 == ""   ]]
    then 
        gcloud compute instances stop $2
    elif [[ $1 == "start" && $2 != "" && $3 == ""   ]]
    then 
        gcloud compute instances start $2
    elif [[ $1 == "delete" && $2 == "project" && $3 != ""   ]]
    then 
        if [ -d /etc/clouder/gcp/accounts/$3 ]
        then
            rm -r /etc/clouder/gcp/accounts/$3
            current=$(cat /etc/clouder/clouder_state | grep project_define)
            if [[ "$current" == *"$3"* ]]
            then
               green_write_sucess "Modification des information dans le fichier clouder_state"
               sed -i "s/$current//g" /etc/clouder/clouder_state
            else
               italic_write "Aucunne modification dans le fichier clouder_state"
            fi
            
        else
            fail_write "Une erreur est survenue le projet n'existe pas"
        fi
    elif [[ $1 == "set" && $2 == "project" && $3 != ""  ]]
    then 
        debug $1 $2 $3 $4 $5 $6
        if [ -d /etc/clouder/gcp/accounts/$3 ]
        then
            important_write "Changement du projet sur $3 "
            green_write_sucess "Le projet existe"
            
            target=$(cat /etc/clouder/clouder_state | grep project_define)
            if [[ "$target" == *"$3"* ]]
            then
               important_write "Le projet est déjà définie sur $3 ✔️" && return 1
            else
               important_write "Configuration du projet ... "
            fi

            if grep  "project_define:" /etc/clouder/clouder_state
            then
                important_write "Changement du projet dans le fichier clouder_state"
                target=$(cat /etc/clouder/clouder_state | grep project_define)
                sed -i "s/$target/project_define:$3/g" /etc/clouder/clouder_state
            else
                 echo "project_define:$3" >> /etc/clouder/clouder_state #cat /etc/clouder/clouder_state | grep project_define
            fi
            
            function prepare_project(){
            important_write ".............................................SAUVEGARDE DU PROJET ACTUELLE ............................................."
            

            if [ ! -z "$get_reference_destination_old_path" ]
            then
                    important_write "Sauvegarde du projet actuelle => $(echo $get_reference_destination_old_path | sed "s/-/ /g") <=  vers le répertoire de destination /etc/clouder/gcp/accounts/$get_reference_destination_old_path" 
                    if [ -d "/etc/clouder/gcp/accounts/$get_reference_destination_old_path" ]
                    then
                        cp /etc/clouder/gcp/accounts/main.tf /etc/clouder/gcp/accounts/$get_reference_destination_old_path && success_write "Copie du projet effectué avec succès !"
                    else
                        fail_write "Le projet n'existe pas" &&    return 1
                    fi
            else
                 fail_write "Impossible d'obtenir des informations sur le projet actuelle" && return 1
            fi

             important_write ".............................................IMPORTATION DU PROJET $1............................................."
                if [[ -e "/etc/clouder/gcp/accounts/$1/main.tf" && ! -z $1 ]]
                then
                    italic_write "Mise à jour des information conservée "/etc/clouder/gcp/accounts/$1/main.tf" vers le nouvelle espace de travail /etc/clouder/gcp/accounts/main.tf"
                    cp /etc/clouder/gcp/accounts/$1/main.tf /etc/clouder/gcp/accounts/main.tf 
                else
                    warning_write "Le projet $1 n'a pas de fichier terraform enregistré"
                    important_write "Suppresion du fichier actuelle main.tf"
                    rm /etc/clouder/gcp/accounts/main.tf
                fi
                #verify_existant_element d /etc/clouder 0 1 "msg on" "msg off"
            }

            if grep  "#project_define:" /etc/clouder/gcp/accounts/main.tf
            then
                important_write "Référence de projet définie au niveau du fichier terraform"
                prepare_project $3
            else
                warning_write "Aucunne référence de projet définie au niveau du fichier terraform"
                important_write "Insertion de l'en-tête > #project_define:$3"
                echo "#project_define:$3" >> /etc/clouder/gcp/accounts/main.tf && 
                italic_write "Projet actuelle définie dans le fichier main.tf"
            fi
            VERIF=$(gcloud projects list)
            if [[ "$VERIF" == *"$3"* ]]
            then
               important_write "Le projet est déjà définie sur $3 ✔️"
            else
                auto_config_process gcloud from /etc/clouder/gcp/accounts/$3/key.json #$3 > /dev/null #Cette fonction indique si il faut créer un fichier terraform nouveau ou pas
                if [ -e /etc/clouder/gcp/accounts/key.json ]
                then
                    rm /etc/clouder/gcp/accounts/key.json
                fi
            fi
        fi
    else
        echo "Erreur d'arguments au niveau de la commande create"
    fi
}

function create_tf()
{
if [ "$1" == "update"  ]
then
    process_create
fi
}
function process_create()
{
    if [[ ! -z $get_reference_destination_old_path ]]
    then
            success_write "Fichier terraform déjà référencer" && italic_warning_write "Pas de création nécessaire" &&
            italic_write "Désactivation {{ Uniformize Ressource ==> Fichier déjà uniformiser}}"
    else
            important_write "Fichier terraform non référencer {{ Pas index de sauvegarde }}"
            important_write "Création d'un nouveau fichier"
    cat > /etc/clouder/gcp/accounts/main.tf << 'EOL'
provider "google" {
  credentials = file("credential_file_path.json")
  project = "xxxx-PROJECT-ID-xxxx"
  region = "xxx-PROJECT-REGION-xxx"
  zone = "xxx-PROJECT-ZONE-xxx"
}
data "google_compute_image" "my_image" {
  family  = "ubuntu-2204-lts"
  project = "ubuntu-os-cloud"
}
data "google_compute_image" "debian" {
  family  = "debian-11"
  project = "debian-cloud"
}
resource "google_compute_instance" "terraform" {
  project      = "xxxx-PROJECT-ID-xxxx"
  name         = "solana-dev-server"
  machine_type = "t2d-standard-4"
  zone         = "xxx-PROJECT-ZONE-xxx"
  tags = ["webserver"]
  boot_disk {
    initialize_params {
      image = data.google_compute_image.my_image.self_link
      size = "60"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
  attached_disk {
    source      = google_compute_disk.default.self_link
    device_name = google_compute_disk.default.name
    mode        = "READ_WRITE"
  }
  metadata_startup_script = <<EOF
#!/bin/bash
apt update && apt install wget -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/install.sh && chmod +x install.sh && bash install.sh && . ~/.bashrc
EOF

depends_on = [ google_compute_firewall.firewall, google_compute_firewall.webserverrule,google_compute_disk.default ]
}



resource "google_compute_instance" "minimal" {
  project      = "xxxx-PROJECT-ID-xxxx"
  name         = "minimal-configuration-server"
  machine_type = "t2d-standard-4"
  zone         = "xxx-PROJECT-ZONE-xxx"
  tags = ["webserver"]
  boot_disk {
    initialize_params {
      image = data.google_compute_image.my_image.self_link
      size = "60"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
metadata_startup_script = <<EOF
#!/bin/bash
apt update && apt install wget -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/install.sh && chmod +x install.sh && bash install.sh && . ~/.bashrc
EOF
depends_on = [ google_compute_firewall.firewall, google_compute_firewall.webserverrule ]
}


resource "google_compute_instance" "ultrabasic" {
  project      = "xxxx-PROJECT-ID-xxxx"
  name         = "server-manager"
  machine_type = "e2-small"
  zone         = "xxx-PROJECT-ZONE-xxx"
  tags = ["testserver"]
  boot_disk {
    initialize_params {
      image = data.google_compute_image.my_image.self_link
      size = "10"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
metadata_startup_script = <<EOF
#!/bin/bash
apt update && apt install wget -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/install.sh && chmod +x install.sh && bash install.sh && . ~/.bashrc
EOF
}

resource "google_compute_instance" "ultrax" {
  project      = "xxxx-PROJECT-ID-xxxx"
  name         = "server-manager-ultrax"
  machine_type = "e2-standard-8"
  zone         = "xxx-PROJECT-ZONE-xxx"
  tags = ["testserver"]
  boot_disk {
    initialize_params {
      image = data.google_compute_image.my_image.self_link
      size = "60"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
}

resource "google_compute_instance" "debian" {
  project      = "xxxx-PROJECT-ID-xxxx"
  name         = "server-manager-debian"
  machine_type = "e2-standard-8"
  zone         = "xxx-PROJECT-ZONE-xxx"
  tags = ["testserver"]
  boot_disk {
    initialize_params {
      image = data.google_compute_image.debian.self_link
      size = "60"
    }
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
}

resource "google_compute_firewall" "webserverrule" {
  name    = "gritfy-webserver"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80","443","3000","8999","8900","9200"]
  }
  source_ranges = ["0.0.0.0/0"] # Not So Secure. Limit the Source Range
  target_tags   = ["webserver"]
}


resource "google_compute_firewall" "firewall" {
  name    = "gritfy-firewall-externalssh"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["0.0.0.0/0"] # Ici on autorise les connexion SSH à partir de n'importe qu'elle adresse IP
  target_tags   = ["externalssh"] #Toutes les vm qui ont la même target_tags appliquent les règles de parefeu.
}
resource "google_compute_disk" "default" {
  name  = "solana-dev-server-disk"
  type  = "pd-ssd"
  zone  = "xxx-PROJECT-ZONE-xxx"
  size = 40
  labels = {
    environment = "dev"
  }
  physical_block_size_bytes = 4096
}
EOL
    fi

}


















































function install_struct()
{
    function ElasticInstall()
    {
        curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo gpg --dearmor -o /usr/share/keyrings/elastic.gpg && echo "deb [signed-by=/usr/share/keyrings/elastic.gpg] https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list && sudo apt update && sudo apt install elasticsearch && echo "Elasti configuration => sudo nano /etc/elasticsearch/elasticsearch.yml " && targetHost=$(cat /etc/elasticsearch/elasticsearch.yml | grep network.host) && sed -i "s/$targetHost/network.host: $1/g" /etc/elasticsearch/elasticsearch.yml && targetPort=$(cat /etc/elasticsearch/elasticsearch.yml | grep http.port) && sed -i "s/$targetPort/http.port: $2/g" /etc/elasticsearch/elasticsearch.yml &&
        sudo systemctl start elasticsearch && sudo systemctl enable elasticsearch
    }
    function KibanaInstall()
    {
        
        echo "process to install Kibana" &&
        sudo apt install kibana -y &&  sudo systemctl enable kibana && sudo systemctl start kibana && 
        sudo apt install nginx -y && echo "$7:`echo $9  | openssl passwd -apr1 -stdin`" | sudo tee -a /etc/nginx/htpasswd.users &&
        cat > /etc/nginx/sites-available/${11} << EOF
        server {
            listen "$5";

            server_name "$3";

            auth_basic "Restricted Access";
            auth_basic_user_file /etc/nginx/htpasswd.users;

            location / {
                proxy_pass http://localhost:5601;
                proxy_http_version 1.1;
                proxy_set_header Upgrade "\$http_upgrade";
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host "\$host";
                proxy_cache_bypass "\$http_upgrade";
            }
        }
EOF
        verify_existant_element__v3 e /etc/nginx/sites-enabled/${11} 1 0 && sudo ln -s /etc/nginx/sites-available/${11} /etc/nginx/sites-enabled/${11}
        sudo nginx -t && sudo systemctl reload nginx && apt install ufw -y && sudo ufw allow 'Nginx Full'
    }
    function LogstashInstall()
    {
        echo "Install Logtash" &&
        sudo apt install logstash -y &&
        cat > /etc/logstash/conf.d/02-beats-input.conf << EOF
                input {
        beats {
            port => $3
        }
        }
EOF
        cat > /etc/logstash/conf.d/30-elasticsearch-output.conf << EOF
        output {
        if [@metadata][pipeline] {
            elasticsearch {
            hosts => ["${10}"]
            manage_template => false
            index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
            pipeline => "%{[@metadata][pipeline]}"
            }
        } else {
            elasticsearch {
            hosts => ["${10}"]
            manage_template => false
            index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
            }
        }
        }      
EOF
        sudo -u logstash /usr/share/logstash/bin/logstash --path.settings /etc/logstash -t && sudo systemctl start logstash && sudo systemctl enable logstash
    }
    function FileBeatInstall()
    {
        echo "Install Filebeat"
        sudo apt install filebeat -y &&
        important_write "Définition des envoie de logs vers Logstash et non ElasticSearch par défauts" &&
        target=$(cat /etc/filebeat/filebeat.yml | grep "output.elasticsearch:") &&
        sed -i "s/$target/#$target/g" /etc/filebeat/filebeat.yml && 
        sed -i 's/hosts: \["localhost:9200"\]/#hosts: ["localhost:9200"]/' /etc/filebeat/filebeat.yml &&
        target1=$(cat /etc/filebeat/filebeat.yml | grep "#output.logstash:") &&
        sed -i "s/$target1/output.logstash:/g" /etc/filebeat/filebeat.yml && 
        sed -i "s/#hosts: \[\"localhost:5044\"\]/hosts: [\"$7\"]/g" /etc/filebeat/filebeat.yml &&
        important_write "Activation du module système de Filebeat" && 
        sudo filebeat modules enable system &&
        important_write "Liste des modules de Filbeat activer : " &&
        sudo filebeat modules list &&
        italic_write "Configuration des pipelines d'ingestion Filebeat, qui analysent les données du journal avant de les envoyer via logstash à Elasticsearch." &&
        sudo filebeat setup --pipelines --modules system
        important_write "Chargement du modèle d'index dans ElasticSearch" && 
        sudo filebeat setup --index-management -E output.logstash.enabled=false -E "output.elasticsearch.hosts=['$3']" &&
        important_write "Création d'un modèle d'index afin de charger les tableaux de bord dans Kibana" &&
        italic_write "Lors du chargement des tableaux de bord, Filebeat se connecte à Elasticsearch pour vérifier les informations de version. Pour charger les tableaux de bord lorsque Logstash est activé, desactivation de  la sortie Logstash et activation la sortie Elasticsearch :" &&
        sudo filebeat setup -E output.logstash.enabled=false -E "output.elasticsearch.hosts=['$3']" -E setup.kibana.host=localhost:5601 &&
        sudo systemctl start filebeat &&
        sudo systemctl enable filebeat &&
        warning_write "Test des envoie FileBeat via ElasticSearch" &&
        curl -XGET "http://$3/filebeat-*/_search?pretty" &&
        success_write "Installation de FileBeat effectué avec succès ! Rendez-vous dans Kibana ! "
    }
    function dbk
    {
        debug $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21}
    }
    if [ "$1" == "terraform" ]
    then
        verify_state "terraform_install_true" 1 0 terraform --v &&
        important_write "Installatiob de terraform" &&
        requires gpg &&
        apt update &&
        apt-get install -y gnupg software-properties-common &&
        wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg &&
        gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint &&
        echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list &&
        apt update &&
        apt-get install terraform 
    elif [ "$1" == "nano" ]
    then
            verify_state "nano_install_true"  1 0 nano --v &&
            apt install nano -y
    elif [ "$1" == "jq" ]
    then
            verify_state "jq_install_true"  1 0 jq --v &&
            apt install jq -y
    elif [[ "$1" == "gitlab" && "$2" == "--host" && "$3" != "" && "$4" == "--reconfigure" ]]
    then
        echo "Gitlab Install" && sudo apt-get update && sudo apt-get install -y curl openssh-server ca-certificates tzdata perl && sudo apt-get install -y postfix && curl -LO https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.deb.sh && sudo bash script.deb.sh && sudo    apt install gitlab-ce && sed -i "s/http:\/\/gitlab.example.com/http:\/\/$3/g" /etc/gitlab/gitlab.rb && sudo gitlab-ctl reconfigure &&   success_write "Installation de Gitlab réussie" && fail_write  "Mot de passe par défauts : " &&  cat /etc/gitlab/initial_root_password | grep Password 
    # Exemple : clouder install gitlab --url 192.168.1.9 --reconfigure
    elif [[ "$1" == "elastic" && "$2" == "--host" && "$3" != "" && "$4" == "--port" && "$5" != "" ]]
    then
          #clouder install elastic --host 123.128.124.120 --port 9200
          ElasticInstall $3 $5   
    elif [[ "$1" == "kibana" && "$2" == "--host" && "$3" != "" && "$4" == "--ExposedPort" && "$5" != "" && "$6" == "--user" && "$7" != "" && "$8" == "--password" && "$9" != "" && "${10}" == "--domain" && ${11} != "" ]]
    then
          #clouder install kibana --host 192.168.1.2 --ExposedPort 9999 --user admin --password offline --domain elastic
          KibanaInstall $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15}
          #dbk $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21} 
    elif [[ "$1" == "logstash" && "$2" == "--listenPort" && "$3" != "" && "$4" == "--sendData" && "$5" == "from" && "$6" == "beats" && "$7" == "to"  && "$8" == "Elastic" && "$9" == "--host" && "${10}" != "" ]]
    then 
        #clouder install logstash --listenPort 5044 --sendData from beats to Elastic --host localhost:9200
        LogstashInstall $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10}
        #dbk $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11} ${12} ${13}  ${14} ${15} ${16} ${17} ${18} ${19} ${20} ${21} 
    elif [[ "$1" == "filebeat" && $2 == "--IndexOutputElasticHost" && "$3" != "" && "$4" == "--connect" && "$5" == "logstash" && "$6" == "--host" && "$7" != ""  ]]
    then 
            #clouder install filebeat --IndexOutputElasticHost localhost:9200 --connect logstash --host localhost:5044
            FileBeatInstall $1 $2 $3 $4 $5 $6  $7 $8    
                 
                           
    elif [ "$1" == "gcloud" ]
    then
        process="$(which gcloud)"
        if [[  "$process" == "" ]]
        then
             echo  "gcloud n'est pas installé ↖"
            verify_state "gcloud_install_true"  1 0 gcloud --v &&
            requires python3 curl tar &&
            (cd /home ; mkdir gcli) &&
            (cd /home/gcli ; curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-cli-393.0.0-linux-x86_64.tar.gz) &&
            (cd /home/gcli ; tar -xf google-cloud-cli-393.0.0-linux-x86_64.tar.gz) &&
            (cd /home/gcli/google-cloud-sdk ; "Y" | ./install.sh)
            source /home/gcli/google-cloud-sdk/completion.bash.inc
            source /home/gcli/google-cloud-sdk/path.bash.inc
        else
            echo "Gcloud est installé"
        fi
    elif [ "$1" == "solger" ]
    then
        apt update && apt install wget -y && wget https://gitlab.com/Ashwin_MK974/solana/-/raw/main/install.sh && chmod +x install.sh && bash install.sh && . ~/.bashrc
    elif [ "$1" == "megatools" ]
    then
        verify_state "megatools_install:true" 1 0 megatools &&
        apt update && apt install megatools -y
    elif [[ "$1" == "gitlab" && "$2" == "--dns" && "$3" != "" ]]
    then
        echo "Gitlab Install"
    elif [ "$1" == "prometheus-stack" ]
    then
        verify_state "prometheus_install:true" 1 0 prometheus &&
        requires tar sudo systemd && 
        important_write "Installation de la stack Prometheus Grafana"
        (cd /etc/clouder ; wget -q https://gitlab.com/Ashwin_MK974/iac-deployment/-/archive/main/iac-deployment-main.tar && tar -xf iac-deployment-main.tar && rm iac-deployment-main.tar ) && 
        (cd /etc/clouder/iac-deployment-main ; mv prometheus-install /etc/clouder/  ) &&
        rm -r /etc/clouder/iac-deployment-main &&
        (cd /etc/clouder/prometheus-install/ ; ./full_installation.sh )
    elif [ "$1" == "minikube" ]
    then
        #verify_state "minikube_install:true"  1 0 minikube --v &&
        requires docker.io
        requires snap
        verify_existant_element__v3 e /etc/clouder/minikube-linux-amd64 1 0 &&
        (cd /etc/clouder ; curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && install minikube-linux-amd64 /usr/local/bin/minikube   )
        if [[ $2 == "" && $3 == "" ]]
        then
            echo "===========================> $2"
            echo "============================> $3"
             warning_write "Veuillez renseigner un nouveau nom d'utilisateur et un  mot de passe"
             italic_write "Exemple : clouder install minikube rev9 offline"
             italic_write "Pour créer un utilisateur rev9 avec le mot de passe offline"
        elif [[ $2 != "" && $3 != "" ]]
        then
            important_write "Création de l'utilisateur $2"
            useradd -m -p $3 -s /bin/bash $2
            important_write "Exécution avec l'utilisateur  $2"
            important_write "Définition des groupes" &&
            usermod -aG docker $2  && newgrp docker && 
            success_write "Définition  des groupes réussie" &&
            snap insatll kubectl --classic &&
            apt install bash-completion  && 
            source <(kubectl completion bash) &&
            echo "source <(kubectl completion bash)" >> /home/$2/.bashrc 
        elif [[ $2 != "" && $3 == "" ]]
        then
            if id "$2" &>/dev/null
            then
                success_write "Utilisateur trouvé"
                usermod -aG docker $2  && newgrp docker
                snap insatll kubectl --classic
                apt install bash-completion
                source <(kubectl completion bash)
                echo "source <(kubectl completion bash)" >> /home/$2/.bashrc
            else
                warning_write "L'utilisateur specifié >  $2 <  n'a pas été trouvé "

            fi
        else
          warning_write "Erreur provenant de l'utilisateur Minikube"
        fi

   

    else
        fail_write "Erreur provenant de la structure install, l'argument $1 n'existe pas"
        warning_write "Clouder ne peut pas installer $1"
    fi
}


function clouder_update(){
if [ "$1" == "tf" ]
then
    important_write "Mise à jour du fichier tf "
    create_tf update
    green_write_sucess "Fichier mise à jour avec succès"
else
        #Vérification de l'installer de clouder
        function get__f(){
    ( cd /etc/clouder ; wget -q https://gitlab.com/Ashwin_MK974/iac-deployment/-/raw/main/clouder.sh && chmod +777 clouder.sh; )
        }
        if [ -d "/etc/clouder" ]
        then
            echo "Detection d'une installation existante de Clouder "
            echo "Mise à jour Clouder ..."
            verify_existant_element__v3 e  /etc/clouder/clouder.sh 0 1 && rm /etc/clouder/clouder.sh;
            create_tf
            get__f;
            bash /etc/clouder/./clouder.sh clouder_init --version
            echo "Mise à jour de clouder réussie"
        else
            echo "Installation en cours"
            mkdir /etc/clouder;
            get__f;
            bash /etc/clouder/./clouder.sh clouder_init --version
            echo "Mise à jour de clouder réussie"
        fi 
fi
}




function k8s()
{
    kubectl create deployment mydeployment --image
}



function k8sGraphical()
{
    requires libxss-dev
    curl -fSL https://github.com/lensapp/lens/releases/download/v4.0.6/Lens-4.0.6.AppImage -o ~/Lens.AppImage
    chmod +x ~/Lens.AppImage
    ~/Lens.AppImage &


        echo "alias src='source ~/.bashrc'">> ~/.bashrc;
        echo "alias k='kubectl'">> ~/.bashrc;
}
function sourcelist(){
    function subprocess()
    {
        cp  /etc/apt/sources.list /etc/clouder/backup/sourcelist && cp -r /etc/apt/sources.list.d/ /etc/clouder/backup/sourcelist
    }
    verify_existant_element__v3 d /etc/clouder/backup 1 0 --s && mkdir /etc/clouder/backup && 
    verify_existant_element__v3 d /etc/clouder/backup/sourcelist 1 0 --s && mkdir /etc/clouder/backup/sourcelist && subprocess
    verify_existant_element__v3 d /etc/clouder/backup/sourcelist 0 1 --s && subprocess
}
#sourcelist


function install_clouder(){

    if  ! grep -Fxq "#clouder:true" ~/.bashrc
    then
        echo "#clouder:true">> ~/.bashrc;
        echo "Creation des alias pour Clouder";
        echo "alias edit-a='nano ~/.bashrc'">> ~/.bashrc;
        echo "alias src='source ~/.bashrc'">> ~/.bashrc;
        echo "alias clouder='bash /etc/clouder/./clouder.sh clouder_init'" >> ~/.bashrc;
        echo "alias clouderexec='bash /etc/clouder/./clouder.sh'" >> ~/.bashrc;
        echo "alias erase='echo '' > '" >> ~/.bashrc;
        echo "#clouder end alias">> ~/.bashrc;
    fi


    if [ ! -d /etc/clouder ]
    then
        important_write "Installation de clouder"
        italic_write "Création du dossier"
        mkdir /etc/clouder
        mkdir /etc/clouder/gcp
        mkdir /etc/clouder/gcp/accounts
        create_tf
        touch /etc/clouder/clouder_state
        cat $PWD/clouder.sh > /etc/clouder/clouder.sh
        italic_write "Copie des fichiers"
        green_write_sucess "Clouder à été installer avec succès"
        rm $PWD/clouder.sh
    fi

    
}
install_clouder
verify_state "sourcelist_copy_true" 1 0 sourcelist --s  && sourcelist
install_struct gcloud  > /dev/null
install_struct terraform > /dev/null
install_struct nano > /dev/null
install_struct jq > /dev/null
<<COMMENT
    => Correction des problèmes de suppresions du fichier main.tf due au mise à jour
COMMENT




"$@"
#verify_state "bash_source:true" 1 0 bashrc  --s && exec bash
#A faire: Mettre en place la fonction auto-config pour le changement de projet

