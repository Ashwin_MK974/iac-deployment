# Clouder a Clouder Manager

  
  

## Installation
```
apt update -y > /dev/null && \
apt install curl wget -y > /dev/null && \
wget -q https://gitlab.com/Ashwin_MK974/iac-deployment/-/raw/main/clouder.sh && \
chmod +x clouder.sh && \
bash clouder.sh
```



  

## Introduction

  
  
  

  
**Introduction**
Clouder est un programme permettant de gèrer avec faciliter les projet sur la plateforme Google Cloud Platform 
**Initialisation**
 Afin de commencer à l'utiliser, il faut au préalable initialiser le projet avec une clé API d'un compte de service gcloud.
 La commande permettant de le faire avec clouder est la suivante:
```
clouder auto-config gcloud from /path/key.json
```
Une fois le projet initialisé un répertoire au niveau de Clouder est créer avec le dossier du projet.
C'est ce dossier qui sera ainsi utilisé afin de pouvoir déployer de nouvelle infrastructure à l'aide de l'outil de provisionning Terraform.

Plusieurs dérive existe sur la fonction d'auto-configuration.
Par exemple elle peut-être auto-provisioné via un lien http comme ci-dessous:
```
clouder auto-config gcloud from  https://mega.nz/xzawHaz
```
Afin de pouvoir obtenir la liste de projet initialisé avec Clouder, il faut lancer la commande ***show project***
```
clouder manage show projects
```
***Résultat de la commande :*** 
 *1. massive-plasma-9181
 2. plucky-sight-361217*
 
Une liste contenant un ou plusieurs projets apparaît alors. Afin de pouvoir sélectionner un projet dans lequelle on va pouvoir travailler dessus, on utilisera alors la commande ***set projet***

```
clouder manage set project plucky-sight-361217
```
Avec la commande ci-dessus, on  à ainsi configurer Clouder afin de pouvoir travailler sur le projet ***plucky-sight-361217***


# I) Création d'infrastructure
Afin de commencer à pouvoir créer des infrastructure, Clouder dispose des préset déjà écrite au niveau du fichier Terraform. C'est dans ce fichier qu'on aura toutes les segments de configuration disponibles que l'on pourra appliquer au niveau de notre projet.

Lorsque le projet à été initialiser au départ, toutes les informations relative à cette clé ont été réporter au niveau du fichier Terraform. Cela permet ainsi d'avoir l'outil qui paré au déploiement afin de pouvoir implémenter de nouvelle infrastructure.
Ainsi ce sont des information comme le nom de la région, le nom du projet, la zone qui sont ainsi reporter au niveau de ce fichier.

**Déploiement en  préset**
Voici les instructions afin de pouvoir déployer des serveurs en préset:
```
clouder deploy server-manager-ultrax
```
Cette commande déploiera un serveur de management dans le projet.
***Spécification du serveur:*** *e2-standard-8*

## I.I) Création de modèle d'instance

Clouder intègre des outils permettant de créer ses propres modèles d'instances et les intégrer dans terraform.
Cela permet ainsi de pouvoir créer des modèle sans avoir toute la configuration syntaxique à implémenter dans le fichier.
La commande suivante permet ainsi de créer un modèle d'instance :

```
clouder create model --name myModels \
--vm-name testnet \
--machine-type e2-micro \
--zone europe-west1-b \
--server-tags testnet \
--disk-size 80 \
--image-source debian

```
Les options comme leurs nom l'indique sont ainsi simple à appréhender.

Voici quelques explications au niveau des commandes utilisés :
***--name*** : Un identifiant à appliquer pour le modèle d'instance

***--project-name*** : Le nom du projet

***--vm-name*** : L'identifiant qui sera utilisé pour la création de la machine virtuelle

***--machine-type*** : Le type de machine dans laquelle le serveur sera implémenté.

***--zone*** : La zone dans laquelle sera déployer le serveur


***--tags*** : Le tags à appliquer sur le serveur, il s'agit d'un identifiant qui est utilisé par
                      le pare-feu afin d'appliquer certaines règles.

***--images-sources*** :   L'image qu'on souhaite utilisé afin de déployer notre machines virtulle.     

**Version**

A la version actuelle de clouder, toutes ces options doivent être obligatoirement définie dans un ordre de séquencement respecté.

## I.2) Création de machine virtuelles basées sur les modèles d'instance
Pour pouvoir exploiter ces modèle d'instances  qui ont étés créer, la commande ***create vm*** permet de créer de nouvelles machines virtuelles basés sur ces modèles.
Exemples:
```
clouder create vm myVm based on myDefaultModel
```
Avec la commande ci-dessus, c'est ainsi une nouvelle machine virtuelles qui est créer. Cela avec toutes les spécifications techniques qui ont étés implémentés dans la partie *I.1) Création de modèle d'instance*.

****I.2.1**) Modification de la machines virtuelles sur le modèle d'instance**
La définition d'un modèle est de pouvoir s'appuyer dessus pour créer de nouvelle infrastructures rapidement, mais aussi de pouvoir les modifier. Pour cela lors de la création, on peut ainsi spécifier si l'on souhaite les éléments que l'on souhaite modifier.

Par exemple si l'on souhaite modifier, le type de machine toute en conservant les caractéristiques du modèle d'instance, la commande est la suivante:
```
clouder create vm myVm based on myDefaultModel \
--machine-type e2-standard-32
```
La commande ci-dessous, créer donc une nouvelle machine virtuelle basé sur un modèle d'instance, mais en spécifiant uniquement une nouvelle plateforme de type de machine.

On peut également enchainer les modification suivant toutes les spécification que l'on à établie dans le modèle.   Si l'on veut par exemple changer le **type de machine** ainsi que le **tags** serveur on exécutera la commande ci-dessous:
```
clouder create vm myVm based on myDefaultModel \
--machine-type e2-standard-32 \
--tags testserver
```
De la même manière si l'on souhaite également changer la taille du disque:

```
clouder create vm myVm based on myDefaultModel \
--machine-type e2-standard-32 \
--tags testserver \
--disk-size 220
```
Possibilité d'avoir du provisioning sur les vm déployés :


```
clouder create vm myVm based on myDefaultModel \
--machine-type e2-standard-32 \
--tags testserver \
--disk-size 220
```

Les options peuvent être répartit alétoirement dans la comamnde.

## I.2) Déploiement de machine virtuelles basées sur les modèles d'instance

Afin de pouvoir déployer les machines virtuelles créer sur les modèles d'instances, on dispose de deux options.
La première étant à la création de la machine virtuelle basé sur l'instance en spécifiant par exemple, l'option              **--deploy**.
Exemple:
```
clouder create vm myVm based on myDefaultModel \
--disk-size 80 --deploy
```
La commande ci-dessus, créer donc une machine virtuelles basé sur le modèle "**myDefaultModel**", en spécifiant cependant une taille de 80 GB de disque. La machine virtuelle sera ainsi déployé automatiquement car elle est suivie de l'argument **--deploy.**

La deuxième options consiste à démarrer la machine virtuelle après la création.
Par exemple:
```
clouder deploy myVm 
```
## I.3) Gestion d'instances
Pour lister la liste des machine que l'on à déployer sur le projet, on va utiliser l'option **manage**
```
clouder manage show vm

```

## I.3) Installation des service
Prometheus - Grafana
```
clouder install prometheus-stack
```
Gitlab
```
clouder install gitlab --host 123.198.124.120 --reconfigure
```
ElasticSearch
```
clouder install elastic --host 123.198.124.120 --port 9200
```
Kibana
```
clouder install kibana --host 123.198.124.120 --ExposedPort 2222 --user admin --password offline --domain elastic
```
Logstash
```
clouder install logstash --listenPort 5044 --sendData from beats to Elastic --host localhost:9200
```
FileBeat
```
clouder install filebeat --IndexOutputElasticHost localhost:9200 --connect logstash --host localhost:5044
```

