terraform {
  required_providers {
    digitalocean = {
      source  = "digitalocean/digitalocean"
      version = "2.21.0"
    }
  }
}
variable "do_token" {
  default = "/etc/solger/do"
}

variable "init" {
  type = string
}
provider "digitalocean" {
  # Configuration options
  token = var.do_token
}
# Create a new SSH key
resource "digitalocean_ssh_key" "default" {
  name       = "Terraform Example"
  public_key = file("/root/.ssh/id_rsa.pub")
}
resource "digitalocean_droplet" "web" {
  image    = "ubuntu-18-04-x64"
  name     = "web-3"
  region   = "nyc3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [digitalocean_ssh_key.default.fingerprint]

}


output "droplet_ip_address" {
  value     = digitalocean_droplet.web.ipv4_address
  sensitive = false
}


#Deployment Commands : terraform apply -var-file="vars.tfvars" -auto-approve
#Destroy Commands :    terraform destroy -var-file="vars.tfvars" -auto-approve
#Les ressources définissent les objects mise à diposition par les clouds provider
